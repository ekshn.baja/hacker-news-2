import React, { Component } from 'react'
import { connect } from 'react-redux'
import Story from './story/Story'

import './Stories.scss'
import Actions from '../../store/actions'


class Stories extends Component {
	state = { 
		error_msg: ''
	}

	componentDidMount() {
		this.props.clearCommentsData()
	}

	render() {
		const story_ids_list = (
			this.props.stories.story_ids_slice.map( (storyID) => (
				<li key={storyID}> <Story id={storyID} ></Story> </li>
			))
		)
		return (
			<ul className='list-of-ids'> { story_ids_list } </ul>
		)
	}
}

const mapStateToProps = state => {
	return {
		stories: {
			story_ids_slice: Object.keys( state.stories.stories_slice_data )
		}
	}
}

const mapDispatchToProps = dispatch => {
	return {
		clearCommentsData: () => dispatch({
			type: Actions.CLEAR_COMMENT_DATA
		})
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(Stories) 