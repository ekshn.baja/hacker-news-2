import React, { Component } from 'react'
import { connect } from 'react-redux'
import Axios from 'axios';
import Actions from '../../../../store/actions'
import Comment from './comment/Comment'

import './Comments.scss'

class Comments extends Component {
	state = {
		error_msg: ''
	}
		
	componentDidMount() {
		let comment_ids = []
		let request_url = ''
		
		if (this.props.parent_type === "story"){
			comment_ids = this.props.stories.stories_slice_data[ this.props.parent_id ].comment_ids
			
			for (let comment_id of comment_ids){
				request_url = 'https://hacker-news.firebaseio.com/v0/item/'+comment_id+'.json?print=pretty'
				Axios.get( request_url )
					.then(Response => {
						this.props.updateCommentsData( 
							this.props.parent_id.toString(), Response.data 
						)
					})
					.catch(error => {
						if (error) this.setState({ ...this.state, error_msg: 'Error something...' })
					})
			}
		}
		else if (this.props.parent_type === "comment"){
			request_url = 'https://hacker-news.firebaseio.com/v0/item/'+this.props.parent_id+'.json?print=pretty'
			// console.log( request_url )
			Axios.get( request_url )
				.then( Response => {
					comment_ids = ('kids' in Response.data)? Response.data.kids : [] 
					
					for (let comment_id of comment_ids){
						request_url = 'https://hacker-news.firebaseio.com/v0/item/'+comment_id+'.json?print=pretty'
						Axios.get( request_url )
							.then( response => {
								this.props.updateCommentsData( 
									this.props.parent_id.toString(), response.data 
								)
							})
							.catch(error => {
								if (error) this.setState({ ...this.state, error_msg: 'Error something...' })
							})
					}
		
				})
				.catch(error => {
					if (error) this.setState({ ...this.state, error_msg: 'Error something...' })
				})
		}
	}

	render() {
		let comments_jsx = null
		let comments_data = this.props.comments[ this.props.parent_id.toString() ]
		if (!comments_data) return comments_jsx

		comments_jsx = (
			<div className='comments-container'>
				{Object.keys( comments_data ).map( id => (
					<Comment key={id} data={comments_data[id]} ></Comment>
				))}
			</div>
		)
		return comments_jsx
	}
}

const mapStateToProps = state => {
	return {
		stories: {
			stories_slice_data: state.stories.stories_slice_data
		},
		comments: state.comments
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateCommentsData: (parent_id, comment_data) => dispatch({
			type: Actions.UPDATE_COMMENT_DATA, parent_id: parent_id, comment_data: comment_data
		})
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(Comments)