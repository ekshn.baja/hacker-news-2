import React, { Component } from 'react'
import { connect } from 'react-redux'
import Actions from '../../../../../store/actions'
import Axios from 'axios'
import Comments from '../Comments'

import './Comment.scss'


class Comment extends Component {
	render() {
		const dt = new Intl.DateTimeFormat("en-GB", {
			year: "numeric",
			month: "long",
			day: "2-digit",
			hour: "numeric",
			minute: "numeric"
		}).format(this.props.data.datetime)

		// console.log( this.props.data )

		const comment_jsx = (
			<div className='comment-container'>
				<div>
					<span> <label> Posted by: </label> {this.props.data.by} </span>
					<span> <label> date: </label> {dt} </span>
				</div>
				<p className='comment-text'> {this.props.data.text} </p>
				
				{/* <Comments parent_id={this.props.data.parent} parent_type='comment' ></Comments> */}
				<Comments parent_id={this.props.data.id} parent_type='comment' ></Comments>
			</div>
		)
		return comment_jsx 
	}
}

export default Comment