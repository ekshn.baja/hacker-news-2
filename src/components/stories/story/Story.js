import React, { Component } from 'react';
import { connect } from 'react-redux'
import Axios from 'axios';

import Actions from '../../../store/actions'
import Comments from './comments/Comments'

import './Story.scss'

class Story extends Component {
	state = {
		error_msg: '',
		show_comments: false
	}

	componentDidMount() {
		const request_url = 'https://hacker-news.firebaseio.com/v0/item/'+this.props.id+'.json?print=pretty'
		Axios.get( request_url )
			.then(Response => {
				this.props.updateStoryData( Response.data )
			})
			.catch(error => {
				if (error) this.setState({ ...this.state, error_msg: 'Error something...' })
			})
	}

	toggleComments = (story_id) => {
		this.setState({ show_comments: !this.state.show_comments })
	}

	render() {
		const story_data = this.props.stories.stories_slice_data[ this.props.id ]
		if (Object.keys(story_data).length === 0) return null
		
		const dt = new Intl.DateTimeFormat("en-GB", {
			year: "numeric",
			month: "long",
			day: "2-digit",
			hour: "numeric",
			minute: "numeric"
		}).format(story_data.datetime)
		const num_comments = story_data.total_num_comments

		let comments_jsx = null
		if (this.state.show_comments){
			comments_jsx = (
				<Comments parent_id={this.props.id} parent_type='story' ></Comments>
			)
		}

		const story_data_jsx = (
			<div className='story-data'>
				<a href={story_data.url} target="_blank"> {story_data.title} </a>
				<div>
					<span> <label>by:</label> {story_data.author} </span>
					<span> <label>score:</label> {story_data.score} </span>
					<span> <label>date:</label> {dt} </span>
					<span className='comment-openner' 
						  onClick={() => this.toggleComments(this.props.id)}> 
						<label className='comment-openner'>comments: </label>{num_comments}
					</span>
				</div>
				{comments_jsx}
			</div>
		)
		return story_data_jsx
	}
}

const mapStateToProps = state => {
	return {
		stories:{
			stories_slice_data: state.stories.stories_slice_data
		} 
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateStoryData: (story_data) => dispatch({
			type: Actions.UPDATE_STORY_DATA, story_data: story_data
		})
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(Story)