import React, { Component } from 'react'
import Axios from 'axios';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import Stories from './stories/Stories'
import Actions from '../store/actions'

import './App.scss'

class App extends Component {
	state = { error_msg: '' }

	componentDidMount() {
		Axios.get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
			.then(Response => {
				this.props.updateStoryIDs( Response.data )
				const url_page_num = this.parseURLforPageNum()
				this.props.gotoStoriesPage( url_page_num )
			})
			.catch(error => {
				if (error) this.setState({ ...this.state, error_msg: 'Error something...' })
			})
	}

	gotoStoriesPage = ( page_num ) => {
		this.props.gotoStoriesPage( page_num )
		this.props.history.push({ pathname:'/page/'+page_num })
	}

	parseURLforPageNum = () => {
		const match = window.location.href.match( /[/]page[/](\d+)/ )
		let page_num = 1
		if (match){
			const param = match[1]
			if (!isNaN(param)) page_num = parseInt(param)
		} 
		return page_num
	}

	render() {
		let page_nums_list = null

		if (this.props.app.total_num_pages > 0){
			const page_nums = [...Array(this.props.app.total_num_pages).keys()].map( item => item+1 )
			
			page_nums_list = (
				page_nums.map( page_num => {
					const LinkClass = ( page_num === this.props.app.current_page_num )?"CompLink-active":"CompLink"
					return (
						<li 
							key={page_num} 
							className={LinkClass} 
							onClick={() => this.gotoStoriesPage(page_num)} > {page_num} </li>
					)
				}		
			))
		}

		return (
			<div className="App">
				<h1> Hacker News </h1>
				
				<ul className='list-of-page-nums'> 
					<li className='page-label'> <span>Page:</span> </li> {page_nums_list}
				</ul>
				
				<Stories></Stories>
				
				<ul className='list-of-page-nums'> 
					<li className='page-label'> Page: </li> {page_nums_list}
				</ul>

				<p> 2020 - Stefan Videnović </p>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		app: {
			maxnum_stories_per_page: state.app.maxnum_stories_per_page,
			current_page_num: state.app.current_page_num,
			total_num_pages: state.app.total_num_pages
		}
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateStoryIDs: (story_ids) => dispatch({
			type: Actions.UPDATE_STORY_IDS, story_ids: story_ids
		}),
		gotoStoriesPage: (page_num) => dispatch({
			type: Actions.UPDATE_STORY_IDS_SLICE, page_num: page_num,
		})
	}
}

export default withRouter( connect( mapStateToProps, mapDispatchToProps )(App) )
