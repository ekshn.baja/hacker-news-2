
const clip_num = ( num, min, max ) => Math.min( Math.max( num, min), max )

const calculate_num_slices = ( total_num_items, maxnum_items_per_slice ) => {
    const num_slices = Math.ceil( total_num_items / maxnum_items_per_slice )
    return num_slices
}

const get_items_slice = ( all_items, slice_i, maxnum_items_per_slice ) => {
    const total_num_items = all_items.length
    const start_i = clip_num( (slice_i-1)*maxnum_items_per_slice, 0, total_num_items )
	const end_i = clip_num( start_i + maxnum_items_per_slice, 0, total_num_items )
	
	const items_slice = all_items.slice( start_i, end_i )
    return items_slice
}

export { calculate_num_slices, get_items_slice, clip_num }